<p align="center">
	<img alt="The Hobania Project logo on a screenshot" src="/assets/voxygen/background000.png">
</p>



#### Official WIKI 

https://gitlab.com/HobaniaProject/thp/-/wikis/home



## Welcome To The Hobania Project!

The Hobania Project is a multiplayer voxel RPG written in Rust. The Hobania Project takes inspiration from games such as Cube World, Minecraft and Dwarf Fortress. The game is currently under heavy development, but is playable.

## Development

Currently the game gets developed by ceo_hobo/mrHobo in spare time.It is a copy of source from another -Git-RePo- on its way to being mangled into something good. This is a personal project and one human can only do as much. Please use the bug tracker for reporting bugs and be patient. a solution will be found.

## Useful Links

[Sign Up](https://hobania.mitmotion.co.za/account/) - Here you can create an online account for The Hobania Project.
This will be needed to play on auth-enabled servers, including the official server.

[The Book](https://hobania.mitmotion.co.za/book) - A collection of all important information relating to The Hobania Project.

[Future Plans](https://gitlab.com/HobaniaProject/thp/-/milestones) - Go here for information about The Hobania Project's development.

#### Official social media and websites

- [Website](https://hobania.mitmotion.co.za)
- [Discord Server](https://CHANGE)


## Get The Hobania Project

We provide 64-bit builds for Linux, Mac, and Windows, which can be downloaded on the official website:
[https://www.hobania.mitmotion.co.za/download/](https://hobania.mitmotion.co.za/download/)

Due to lack of skill, and a constant fear of messing up a git repo, a decision has been made not to be a direct clone of the original repo an so this game will evolve away from the original source.

or ultimatly crash an burn...

<p align="center">
	<img alt="The Hobania Project server Live ad image" src="/assets/voxygen/banner/Àd-000.png">
</p>


## F.A.Q.


### **Q:** What platforms are supported?

**A:** The Hobania Project can run on Windows, Linux and Mac OS on all architectures (although x86_64 is our main focus). It's probably possible to compile The Hobania Project on/for BSD, Fuchsia and others as well.

### **Q:** Do you accept donations?

**A:** You can support the project on our [OpenCollective Page](https://opencollective.com/mitmotion).

## Credit

Many thanks to everyone that has contributed to the original source of Veloren(veloren.net) development, provided ideas, crafted art, composed music, hunted bugs, created tools and supported the project. So that we can come and fck it up with our ideas. 
